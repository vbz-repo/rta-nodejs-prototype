$(function () {
	var btns = $("form#senddata button");
	//rewrite buttons click event
	for (var i = 0; i < btns.length; i++) {
		$(btns).eq(i).click(function (e) {
			this.value = 1;
			$(this.form).submit();
			this.value = 0;
		});
	}
	
	$("#senddata").submit(function (e) {
		e.preventDefault();
				
		$.post("/senddata", 
		{ osLinux: this["osLinux"].value, osWindows: this["osWindows"].value, osMac: this["osMac"].value },
		function (data) { // success
			$("#senddata fieldset").prop("disabled", true); // disable a form elements
			$("#alert").addClass(data.msgClass); // to alert container add css succes class
			$("#alert button.close").after(data.msg); // added success message
			$("#alert").prop("hidden", false); // show alert container
		});
	})
	
	$('[data-dismiss="alert"]').click(function (e) {
		$("#alert").prop("hidden", true); // hide alert container
		$("#alert p").remove(); // remowe success message
		$("#alert").removeClass().addClass("alert"); // remove success css class 
		$("#senddata fieldset").prop("disabled", false); // create active a form elements
	})
});