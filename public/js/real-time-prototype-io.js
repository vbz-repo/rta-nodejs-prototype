$(function () {
  var socket = io('http://localhost:8080');
  //var socket = io('http://192.168.0.101:8080');

  // update on real time a vote result
  socket.on('senddata', function(data){
    $("#osLinux span.badge").text(data.osLinux);
    $("#osWindows span.badge").text(data.osWindows);
    $("#osMac span.badge").text(data.osMac);
  });
});