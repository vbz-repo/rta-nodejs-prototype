var express = require('express');
var router = express.Router();

var osUsage = {
  osLinux: 0,
  osWindows: 0,
  osMac: 0
};

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { AuthorName: res.app.locals.authorName, AuthorEmail: res.app.locals.authorEmail, osUsage });
});

/* GET send data page. */
router.get('/senddata', function (req, res, next) {
  res.render('senddata', { AuthorName: res.app.locals.authorName, AuthorEmail: res.app.locals.authorEmail });
});

/* POST send data page. */
router.post('/senddata', function (req, res, next) {
  // TODO:
  // In production - requires a strong data validation!

  updateData(osUsage, req.body);
  res.send({
    msg: "<p><strong>Thank you!</strong> Your Choice: " + getKeyByValue(req.body, "1") + "</p>",
    msgClass: "alert-success"
  });

  //emit senddata event
  res.app.get('ioServer').emit('senddata', osUsage);
});

function updateData(o, postdata) {
  o.osLinux += parseInt(postdata.osLinux);
  o.osWindows += parseInt(postdata.osWindows);
  o.osMac += parseInt(postdata.osMac);
};

// get key name in object by value
function getKeyByValue(o, value) {
  try {
    for (var prop in o) {
      if (typeof o[prop] !== 'undefined')
        if (o[prop] === value)
          return prop;
    }
  }
  catch (ex) {
    console.error('[ERROR] getKeyByValue\n', ex.name + ':', ex.message);
  }
};

module.exports = router;
