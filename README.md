## RTA NodeJS Prototype
<!-- ### Version: 0.9.0 -->
### Summary

"RTA NodeJS Prototype" shows the building of real-time applications with Node.js and Socket.io.  
 The prototype shows only the basic work principles Socket.io.

### Prerequisites

* Node.js  
[Download](https://nodejs.org/en/download/) the Node.js source code or a pre-built installer for your platform.  
The [documentation](https://docs.npmjs.com/getting-started/installing-node) to installing Node.js and updating npm.

### Installation

1. cd *local source directory*
2. npm install

### Running the tests

1. cd *local source directory*
2. node bin/www
3. Open in browser the URL: *localhost:8080*
4. Open in the browser another window or tab with the URL: *localhost:8080/senddata*

![Running "RTA NodeJS Prototype"](docs/rta-nodejs-prototype-01.png)

### Authors
* Valdis Bluzma

### License
[MIT](./LICENSE)
